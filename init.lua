-- Author: Astrobe
-- licence: CC-BY-SA 4.0
-- Version 20.07.24.01

local mimic_node="mimic_nodes:node"
local post_effect= {a = 128, r = 255, g = 255, b = 0},

minetest.register_node(mimic_node,
	{
		description="Mimic node",
		drawtype="glasslike",
		tiles={"default_tin_block.png^default_glass.png"},
		groups={ cracky=3, },
		sounds = default.node_sound_glass_defaults(),
		drop=mimic_node,
		paramtype = "light",
		paramtype2 = "glasslikeliquidlevel",
		sunlight_propagates = true,
		is_ground_content = false,
		post_effect_color=post_effect,
		walkable=false,
		on_place=function(itemstack, placer, pointed)
			if pointed.type=="node" then
				local model=minetest.get_node(pointed.under).name
				local mimic="mimic_nodes:"..model.gsub(model, ":", "_")
				if minetest.registered_nodes[mimic] then
					minetest.set_node(pointed.above, {name=mimic})
					itemstack:take_item()
				elseif string.match(model, "^mimic_nodes:") then
					minetest.set_node(pointed.above, {name=model})
				else
					minetest.set_node(pointed.above, {name=mimic_node})
				end
			end
			return itemstack
			end
	})
minetest.register_craft
{
	type="shapeless",
	output=mimic_node,
	recipe={"default:glass", "default:tinblock", "default:mese_crystal_fragment"}
}

function make_mimic(model)
	local name=string.gsub(model, ":", "_")
	minetest.register_node("mimic_nodes:"..name,
	{
		description="Mimic node (active)", -- should not appear in inventory, though.
		drawtype="glasslike",
		tiles=minetest.registered_nodes[model].tiles,
		groups={ cracky=3, },
		sounds = default.node_sound_glass_defaults(),
		drop=mimic_node,
		paramtype = "light",
		paramtype2 = minetest.registered_nodes[model].paramtype2,
		sunlight_propagates = true,
		is_ground_content = false,
		post_effect_color=post_effect,
		walkable=false,
	})
end

for _, name in ipairs {
	"default:dirt",
	"default:sand",
	"default:stone",
	"default:dirt_with_grass",
	"default:cobble",
	"default:mossycobble",
	"default:stonebrick",
	"default:stone_block",
	"default:glass",
	"default:wood",
	"default:gravel",
} do make_mimic(name) end


