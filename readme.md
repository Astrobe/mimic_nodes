Mimic nodes copy the appearance of the nodes you place them on. However, you walk (or fall) through them.

They are therefore perfect to make traps and secret doors. However, a few thing can giveaway their presence:
* they also let light pass through, so if your secret room is lit, the secret entrance may be spotted, especially at
  night.
* they emit a glass sound when you hit them.

Mobs may or may not be fooled by mimic nodes depending on your mob engine.

Falling node fall through mimic nodes, but if a falling node lands where a mimic node is, due to Pauli's exclusion
principle, the mimic node will be dropped.

A limitation is that mimic nodes cannot mimic just any node, we have to create them before hand. See the list at the end
of init.lua. There are a large numer of nodes that you may want to mimic, some of them being in mods I don't even know
about. I have chosen to define mimics for the most common nodes.

Another limitation is that mimics only work on full nodes with the same texture on all faces; for instance, mimicking
tree nodes does not work as expected.

The default recipe includes glass, tin and a mese fragment because mimick nodes are like one-way mirrors.

Licence: CC-BY-SA 4.0
